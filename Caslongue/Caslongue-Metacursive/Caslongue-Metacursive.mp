%Bienvennu·e·s!

% http://192.168.3.136/index.html#elasticgirls
% http://192.168.3.136/index.html#bodybolder
% http://192.168.3.33/index.html#bosobiso
% http://192.168.3.33/index.html#lowcase

% Les lignes commençant par le signe `%` ne sont pas prises en compte par metapost.
% Elles peuvent donc servir à mettre des commentaires :>

% SPECIMEN  http://192.168.3.136/histoire-visuelle-de-l-art-typographique.png

% Des ressources et exemples Metapost en ligne :
% Syracuse doc' : https://melusine.eu.org/syracuse/metapost/mpman/
% Metapost raconté aux piétons, Yves Soulet - cahiers GUTenberg, n°52-53, octobre 2009 http://yves.soulet.free.fr/pub/manuel_pour_metapost_2009.pdf
% The METAFONT tutorial, ChristopheGrandsire - Version 0.33, December 30, 2004 http://metafont.tutorial.free.fr/downloads/mftut.pdf

% Notre point de départ
% Specimen of printing types, fonderie Caslon 1841 : https://archive.org/details/SpecimenOfPrintingTypes/page/n317

% Quelques exemples et situations de lettrages élastiques
% Glasgow roadliners http://www.ostreet.co.uk/project/roadliners-2/
% David Poullard  https://vimeo.com/342943596
% Tokyo subway’s humble duct-tape typographer Shuetsu Sato https://medium.com/@chrisgaul/tokyo-subways-humble-duct-tape-typographer-a8c84bb6b99b et https://www.instagram.com/explore/tags/%E4%BD%90%E8%97%A4%E4%BF%AE%E6%82%A6/

% cheatsheet !!! https://gitlab.com/erg-type/workshop.meta-elastique/blob/master/cheatsheet/cheatsheet.md

% Table Unicode https://unicode-table.com/fr/

% ↓ Ici commence la description de nos dessins


% La ligne suivante indique à Metapost de stocker chaque figure produite dans un dossier nommé ‘svg’
outputtemplate := "svg/%c.svg";
% Pour plus de comodité dans la manipulation des fichiers on précise ici qu'on aimerait avoir des fichiers vectoriels, au format svg en sortie
outputformat := "svg";

% On définit ici la valeur de l'unité de base
u = 40pt;
% On peut faire varier le coefficient de cette valeur en x et en y
ux = 1*u;
uy = 1.6*u;

% Les 3 lignes suivantes permettent d'afficher (1) ou d'éteindre (0) l'affichage des éléments de la grille de dessin
grid= 0;
hints = 0;
dot_label = 0;

height = 14;
baseline := 0;
xHeight := 18;
ascHeight := 12;
descHeight := -4;
capHeight := 11;

% L'épaisseur du trait peut être préciser en x et en y, l'inclinaison de la plume est définie avec une valeur de rotation.
strokeX := 1 * u;
strokeY := 1 * u;
rotation := 90;

% GRAISSES

% traverse :=  (strokeY);

def penTraverse=
	pickup penrazor (strokeY /2);
enddef;

def penCanne=
	pickup penrazor(strokeY /2.7);
enddef;

def penDelie=
	pickup penrazor (strokeY /3);
enddef;

% Les lignes ci-dessous permettent de dessiner la grille en faisant appel aux variables définies plus haut.
def beginchar(expr keycode, width)=
	beginfig(keycode);
		pickup pencircle scaled .2;

		draw (0 * ux, (descHeight - 2) * uy) -- 
			(width * ux, (descHeight - 2) * uy) --
			(width * ux, (ascHeight + 2) * uy) -- 
			(0 * ux, (ascHeight + 2) * uy) -- 
			cycle scaled 0 withcolor red;

		if grid = 1:
			defaultscale := .2;
			for i=0 upto width:
				draw (i*ux, ascHeight*uy) -- (i*ux, descHeight*uy) withcolor .3white;
			endfor;
			for i=descHeight upto (ascHeight):
				draw (width*ux, i*uy) -- (0*ux, i*uy) withcolor .3white;
			endfor;
		fi;
		pickup pencircle scaled 1;

		if hints = 1:
			% draw (0 * ux, (xHeight * uy) + os_x) -- (width * ux, (xHeight * uy) + os_x)  withcolor green;
			% draw (0 * ux, xHeight * uy) -- (width * ux, xHeight * uy)  withcolor (green + blue);

			draw (0 * ux, capHeight * uy) -- (width * ux, capHeight * uy)  withcolor (green + blue);
			draw (0 * ux, ascHeight * uy) -- (width * ux, ascHeight * uy)  withcolor (green + blue);
			draw (0 * ux, descHeight * uy) -- (width * ux, descHeight * uy)  withcolor (green + blue);
			draw (0 * ux, baseline * uy) -- (width * ux, baseline * uy)  withcolor green;
		fi;
		linejoin := mittered;
		linecap := rounded;
		pickup pensquare xscaled strokeX yscaled strokeY rotated rotation;

	enddef;

	% Pour s'y retrouver on peut matérialiser et numéroter les points du tracé.
	def endchar(expr lenDots)=
	if dot_label = 1:
		defaultscale := 3;
		for i=1 upto lenDots:
			dotlabels.urt([i]) withcolor blue;
		endfor;
	fi;
endfig;
enddef;


%TRANSITAL

ital = -1 ;
%dirital = 50;

def transital(suffix npoint) =
	for i=1 upto npoint;
		x[i] := x[i] + (y[i] * ital);

	endfor;
enddef;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%ORDRE ALPHABÉTIQUE

% A
% Pour spécifier qu'il s'agit de notre A capital latin, sa valeur ascii est indiquée, le 9 qui suit correspond à la largeur de la grille (nombre d'unités)

beginchar(65, 9)
	x1 := 2 * ux;
	bot y1 := 0 * uy;
	x2 := 4.5 * ux - (strokeX / 5);
	top y2 := capHeight * uy;
	x3 := 4.5 * ux + (strokeX / 5);
	top y3 := capHeight * uy;
	x4 := 7 * ux;
	bot y4 := 0 * uy;
	x5 := 2.5 * ux ;
	y5 := 2 * uy ;
	x6 := 6.5 * ux;
	y6 := 2 * uy;

	transital(6)

	draw z1 -- z2{dir 20} .. z3 -- z4 ;
	draw z5--z6 ;

endchar(6);

%B
beginchar(66,8)

	x1:= 2 * ux;
	bot y1 := baseline * uy;
	x2 :=  2 * ux;
	top y2 := capHeight * uy;
	x3 := 4 * ux;
	top y3 := capHeight * uy;
	x4 := 4 * ux;
	y4 := 5.5 * uy;
	x5 := 5 * ux;
	bot y5 := baseline * uy;
	x6 := 4.2 * ux;
	top y6 := capHeight * uy;
	x7 := 6 * ux;
	y7 := 5.25 * uy;
	x8 := 2* ux;
	y8 := 5.5* uy;

	transital(8)

	%draw z5--z1--z2--z3 ;
	%draw z3{right}..z6..{left}z4--z8 ;
	%draw z4{right}..z7..{left}z5 ;

	%pickup pencircle scaled (strokeX / 1.53); 

	draw z5 -- z1 -- z2 -- z3{right} .. z6 .. {left}z4 .. z7{right} .. {left}z5 -- cycle ;
	%draw z4 -- z8;

endchar(8)

%C
beginchar(67,8)
	x1 := 6ux;
	y1 := 7.5uy;
	top z2 = (4ux, 11uy);
	z3 = (2ux, 7.5uy);
	z4 = (2ux, 3.5uy);
	x5 := 4ux;
	bot y5 := baseline;
	x6 := 6ux;
	y6 := 3.5uy;

	%x7 := 7ux;
	%y7 := 3.5uy;

	%x8 := 7ux;
	%y8 := 7.5uy;

	transital(6)



	draw z1{dir135} .. {left}z2{left} .. {dir 315}z3 -- z4{dir 315} .. {right}z5{right}.. {dir 135}z6 ;

endchar(6);

%D

beginchar(68,8)
	x1 := 2 * ux;
	bot y1 := baseline * uy;
	x2 := 2 * ux;
	top y2 :=  capHeight * uy;
	x3 := 4.5 * ux;
	top y3 := capHeight * uy;
	x4 := 7.5 * ux;
	y4 := 8 * uy;
	x5 := 7.5 * ux;
	y5 := 3 * uy;
	x6 := 4.5 * ux;
	bot y6 := baseline * uy;

	transital(6)

	draw z4{dir 135}..{left}z3--z2--z1--z6{right}..{dir 135}z5--z4;
endchar(6) ;

%E

beginchar(69,7)
	x1 := 2 * ux;
	bot y1 := 0 * uy;
	x2 := 2 * ux;
	y2 := 5.5 *uy;
	x3 := 2 * ux;
	top y3 := 11 *uy;
	x4 := 5 * ux;
	top y4 := 11 *uy;
	x5 := 5 * ux;
	y5 := 5.5 *uy;
	x6 := 5 * ux;
	bot y6 := 0 *uy;

	transital(6)

	draw z6 -- z1--z2--z3 -- z4 ;

	draw z2--z5 ;

endchar(6)

%F

beginchar(70, 7)
	x1 := 2 * ux;
	bot y1 := baseline * uy;
	x2 := 2 * ux;
	top y2 := capHeight * uy;
	x3 := 5 * ux;
	top y3 := capHeight * uy; 
	x4 := 2 * ux;
	y4 := 5.5 * uy;
	x5 := 5* ux;
	y5 := 5.5 * uy;

	transital(5)

	draw z1 -- z2 -- z3 ;
	draw z4 -- z5 ;

endchar(5);

%G
beginchar(71,8)
	x1:= 6.5 * ux;
	y1 := 7 * uy;
	x2 := 6.5 * ux;
	top y2 := 8.5 * uy;
	x3 := 4 * ux;
	top y3 := capHeight * uy;
	top y4 := 8.5 *uy;
	x4 := 2 * ux;
	x5 := 2 * ux;
	bot y5 := 2.5 * uy;
	x6 := 4 * ux;
	bot y6 := baseline * uy;
	x7 := 6.5 * ux;
	bot y7 := 2.5 * uy;
	x8 := 6.5 * ux;
	y8 := 4 * uy;
	x9 := 5 * ux;
	y9 := 4 * uy;
	x10 :=  6 * ux + (strokeX / 3);
	bot y10 := 0 * uy - (strokeY / 3.6);
	x11 := 6.5 * ux + (strokeX / 3);
	y11 := 4 * uy;
	x12 := 5 * ux + (strokeX / 3);
	y12 := 4 * uy + (strokeY / 3);

	transital(12)

	draw z1--z2 {dir 135}..{left}z3{left} ..{dir 315}z4 --z5 {dir 315}..{right}z6{right}..{dir 135}z7 --z8 --z9 ;
	draw z8 --z9 ;
	penCanne;


endchar(12)

%H
beginchar(72, 8);
	x1 := 2 * ux;
	bot y1 := 0 * uy;
	x2 := 2 * ux;
	top y2 := capHeight * uy;
	x3 := 6 * ux;
	top y3 := capHeight * uy;
	x4 := 6 * ux;
	bot y4 := 0 * uy;
	x5 := 2 * ux;
	y5 := 5.5 * uy;
	x6 := 6 * ux;
	y6 := 5.5 * uy;

	transital(6)

	draw z1 -- z2 ;
	draw z3 -- z4 ;
	pickup pencircle scaled (strokeX / 1.5);
	draw z5 -- z6 ;

endchar(6);

%I

beginchar(73,6)
	x1 := 3 * ux;
	top y1 := 11 * uy;
	x2 := 3 * ux;
	bot y2 := 0 *uy;

	transital(2)

	draw z1--z2 ;

endchar(2);

%J
beginchar(74,9)
	x1 := 6.5 * ux;
	top y1 := capHeight * uy;
	x2:= 6.5 * ux;
	y2:= 3.5 * uy;
	bot x3:= 2.75 * ux;
	bot y3:= 0 * uy;
	x4:= 2 * ux;
	y4:= 3.5 * uy;

	transital(4)

	draw z1 -- z2{dir 315}..{left}z3{left}..{dir135}z4 ;

endchar(4);

%K
beginchar(75, 10)
	x1 := 3 * ux;
	bot y1 := baseline * uy;
	x2 := 3 * ux;
	top y2:= capHeight * uy;
	x3:=7.5ux;
	top y3:=capHeight * uy;
	z4:=(3ux, 3uy);
	z5 := 0.5 [z3,z4];
	bot z6 := (7.5ux, baseline);
	bot y5:=baseline * uy;

	transital(6)

	draw z1--z2 ;
	draw z3--z4 ;
	draw z5--z6 ;

endchar(6);

%L

beginchar(76,8)
	x1 := 2 * ux;
	top y1 := capHeight * uy;
	x2 := 2 * ux;
	bot y2 := baseline * uy;
	x3 := 6 * ux;
	bot y3 := baseline * uy;

	transital(3)

	draw z1--z2--z3 ;

endchar(3);

%M
beginchar(77,12);
	x1 := 2 * ux;
	bot y1 := baseline * uy;
	x2 := 2 * ux;
	top y2 := capHeight * uy;
	x3 := 2 * ux + (strokeX / 5);
	top y3 := 11 * uy;
	x4 := 6 * ux - (strokeX / 8) ;
	bot y4 := baseline * uy;

	x5 := 6 * ux + (strokeX / 8);
	bot y5 := baseline * uy;
	x6 := 10 * ux - (strokeX / 5);
	top  y6 := capHeight * uy;
	x7 := 10 * ux;
	top  y7 := capHeight * uy;
	x8 := 10 * ux;
	bot y8 := baseline * uy;

	transital(8)

	draw z1 -- z2 ;
	draw z6 -- z7 -- z8 ;
	draw z2 .. z3 ;

	draw z2--z3 -- z4 -- z5 -- z6 ;

endchar(8);

% N
beginchar(78, 10);
	x1 := 2 * ux;
	bot y1 := 0 * uy;
	x2 := 2 * ux;
	top y2 := capHeight * uy;
	x3 := 2 * ux + (strokeX / 3);
	top y3 := capHeight * uy;
	x4 := 8 * ux - (strokeX / 3);
	bot y4 := 0 * uy;
	x5 := 8 * ux;
	bot y5 := 0 * uy;
	x6 := 8 * ux;
	top y6 := capHeight * uy;

	transital(6)

	draw z1 -- z2 --  z3 -- z4 -- z5 -- z6  ;

	pickup pencircle scaled (strokeX / 1.5);

endchar(6);

% O
beginchar(79, 8);

	z1 :=(7ux, 7.5uy);



	top z2 = (4ux, 11uy);

	z3 = (2ux, 7.5uy);
	z4 = (2ux, 3.5uy);
	x5 := 4ux;
	bot y5 := baseline;
	x6 := 7ux;
	y6 := 3.5uy;

	transital(6)

	draw z1{dir 135} .. {left}z2{left} .. {dir 315}z3 -- z4 {dir 315} .. {right}z5{right} .. {dir 135}z6 -- cycle ;

endchar(6);

%P
beginchar(80,10)
	x1 := 3 * ux;
	bot y1 := 0 * uy;
	x2 := 3 * ux;
	top y2 := capHeight * uy;
	x3 := 5 * ux;
	top y3 := 11 * uy;
	x4 := 8 * ux;
	y4 := 8 * uy;
	x5 := 5 * ux;
	y5 := 6 * uy;
	x6 := 3 * ux;
	y6 := 6 * uy;

	transital(6)

	draw z1--z2 ;
	draw z2--z3{right}..{dir 315}z4{dir 315}..{left}z5--z6 ;

endchar(6)

% Q
beginchar(81, 8);
	x1 := 6.5ux;
	y1 := 7.5uy;
	top z2 = (4ux, 11uy);
	z3 = (2ux, 7.5uy);
	z4 = (2ux, 3.5uy);
	x5 := 4ux;
	bot y5 := baseline;
	x6 := 6.5ux;
	y6 := 3.5uy;
	x7 := 6ux;
	y7 := 7.5uy;
	x8 := 4ux;
	y8 := baseline;
	x9 := 5ux;
	y9 := -2uy;

	transital(9)

	draw z1{dir 135}..{left}z2{left} .. {dir 315}z3--z4{dir 315}..{right}z5{right}..{dir 135}z6--z1 ;
	draw z8{down}..{right}z9 ;

endchar(9);

%R
beginchar(82,8)
	x1 := 2 * ux;
	bot y1 := 0 * uy;
	x2 := 2 * ux;
	top y2 := capHeight * uy;
	x3 := 4 * ux;
	top y3 := 11 * uy;
	x4 := 6.5 * ux;
	y4 := 8.5 * uy;
	x5 := 4 * ux;
	y5 := 6 * uy;
	x6 := 2 * ux;
	y6 := 6 * uy;
	z7 := 0.8 [ z6, z5];
	x8 := 6 * ux;
	y8 := 3.5 * uy;
	x9 := 6.5 * ux;
	bot y9 := 0 * uy;



	transital(9)


	draw z1--z2 ;
	draw z2--z3{right}..{dir 315}z4{dir 315}..{left}z5--z6 ;
	draw z7--z9 ;

endchar(9)

%S
beginchar(83,8)

	x1 := 4 * ux;
	top y1 := capHeight * uy;
	x2 := 1 * ux;
	y2 := 8.5 * uy;
	x3 := 4 * ux;
	y3 := 6 * uy;
	x4 := 7 * ux;
	y4 := 3 * uy;
	x5 := 4 * ux;
	bot y5 := baseline * uy;
	x6 := 1 * ux;
	y6 := 3 * uy; 
	x7 := 7 * ux;
	y7 := 8.5 * uy;


	transital(7)
	draw z7{dir135}..{left}z1{left}..{dir 315}z2{dir 315}..{right}z3{right}..{dir 315}z4{dir 315}..{left}z5{left}..{dir 135}z6 ;

endchar(7);

%T
beginchar(84, 8)
	x1 :=1 * ux;
	top y1 := capHeight * uy;
	x2 := 4 * ux;
	top y2 := capHeight * uy;
	x3 := 7 * ux;
	top y3 := capHeight * uy;
	x4 := 4 * ux;
	bot y4 := baseline * uy;

	transital(4)

	draw z1 -- z2 -- z4 ;
	draw z2 -- z3 ;

endchar(4);

%U

beginchar(85,8)
	x1 := 7ux;
	top y1 := capHeight * uy;
	x2 := 7ux;
	y2 := 3.5uy;
	x3 := 4.5ux;
	bot y3 := baseline;
	z4 = (2ux, 3.5uy);
	x5 := 2ux;
	top y5 := capHeight * uy;

	transital(5)

	draw z1--z2{dir315}..{left}z3{left}..{dir 135}z4--z5 ;


endchar(5);

% V

beginchar(86,10)
	x1 := 2 * ux;
	top y1 := capHeight * uy;
	x2 := 5 * ux;
	bot y2 := baseline * uy;
	x3 := 8 * ux;
	top y3 := capHeight * uy;

	transital(3)

	draw z1--z2--z3 ;

endchar(3);

%W

beginchar(87,12)
	x1 := 2 * ux;
	top y1 := capHeight * uy;
	x2 := 4 * ux;
	bot y2 := baseline * uy;
	x3 := 4 * ux + (strokeX / 5);
	bot y3 := baseline * uy;
	x4 := 6 * ux;
	top y4 := capHeight * uy;
	x5 := 8 * ux;
	bot y5 := baseline * uy;
	x6 := 8* ux + (strokeX / 5);
	bot y6 := baseline * uy;
	x7 := 10 * ux;
	top y7 := capHeight * uy;

	transital(7)

	draw z1--z2 ;
	draw z2--z3 ;
	draw z5--z6 ;
	draw z6--z7 ;

	draw z3--z4--z5 ;

endchar(7); 
%X
beginchar(88,8)

	x1 := 2 * ux;
	bot y1 := 0 * uy;
	x2 := 8 * ux;
	top y2 := capHeight * uy;
	x3:= 2 * ux;
	top y3:=capHeight * uy;
	x4:= 8 *ux;
	bot y4 := 0 * uy;

	transital(4)

	draw z1--z2;
	draw z3--z4;
endchar(4)

endchar(7);

%Y

beginchar(89,8)
	x1 := 2 * ux;
	top y1 := capHeight * uy;
	x2 := 4 * ux;
	y2 := 4 * uy;
	x3 := 6 * ux;
	top y3 := capHeight * uy;
	x4 := 4 * ux;
	bot y4 := baseline * uy; 

	transital(4)

	draw z1--z2--z3 ;
	draw z2--z4 ;

endchar(4);

%Z
beginchar(90,8)

	x1:= 2 * ux;
	top y1:= capHeight * uy;
	x2:= 7 * ux;
	top y2:= capHeight * uy;
	x3:= 2 * ux;
	bot y3:= 0 * uy;
	x4:= 7 * ux;
	bot y4:= 0 * uy;

	transital(4)

	draw z1--z2--z3--z4 ;

endchar(4)

%ne touchez pas au "end" ci-dessous svplease
end;





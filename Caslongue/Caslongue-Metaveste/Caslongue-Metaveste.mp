% Bienvennu·e·s!

% http://192.168.3.33/index.html#elasticgirls
% http://192.168.3.136/index.html#bodybolder
% http://192.168.3.33/index.html#bosobiso
% http://192.168.3.136/index.html#lowcase

% Les lignes commençant par le signe `%` ne sont pas prises en compte par metapost.
% Elles peuvent donc servir à mettre des commentaires :>

% SPECIMEN  http://192.168.3.136/histoire-visuelle-de-l-art-typographique.png

% Des ressources et exemples Metapost en ligne :
% Syracuse doc' : https://melusine.eu.org/syracuse/metapost/mpman/
% Metapost raconté aux piétons, Yves Soulet - cahiers GUTenberg, n°52-53, octobre 2009 http://yves.soulet.free.fr/pub/manuel_pour_metapost_2009.pdf
% The METAFONT tutorial, ChristopheGrandsire - Version 0.33, December 30, 2004 http://metafont.tutorial.free.fr/downloads/mftut.pdf

% Notre point de départ
% Specimen of printing types, fonderie Caslon 1841 : https://archive.org/details/SpecimenOfPrintingTypes/page/n317

% Quelques exemples et situations de lettrages élastiques
% Glasgow roadliners http://www.ostreet.co.uk/project/roadliners-2/
% David Poullard  https://vimeo.com/342943596
% Tokyo subway’s humble duct-tape typographer Shuetsu Sato https://medium.com/@chrisgaul/tokyo-subways-humble-duct-tape-typographer-a8c84bb6b99b et https://www.instagram.com/explore/tags/%E4%BD%90%E8%97%A4%E4%BF%AE%E6%82%A6/

% cheatsheet !!! https://gitlab.com/erg-type/workshop.meta-elastique/blob/master/cheatsheet/cheatsheet.md

% Table Unicode https://unicode-table.com/fr/

% ↓ Ici commence la description de nos dessins


% La ligne suivante indique à Metapost de stocker chaque figure produite dans un dossier nommé ‘svg’
outputtemplate := "svg/%c.svg";
% Pour plus de comodité dans la manipulation des fichiers on précise ici qu'on aimerait avoir des fichiers vectoriels, au format svg en sortie
outputformat := "svg";

% On définit ici la valeur de l'unité de base
u = 40pt;
% On peut faire varier le coefficient de cette valeur en x et en y
ux = 1*u;
uy = 1.6*u;

% Les 3 lignes suivantes permettent d'afficher (1) ou d'éteindre (0) l'affichage des éléments de la grille de dessin
grid= 0;
hints = 0;
dot_label = 0;

height = 14;
baseline := 0;
xHeight := 18;
ascHeight := 12;
descHeight := -4;
capHeight := 11;

% L'épaisseur du trait peut être préciser en x et en y, l'inclinaison de la plume est définie avec une valeur de rotation.
strokeX := 3 * u;
strokeY := 1.5 * u;
rotation := 0;

% GRAISSES

% traverse :=  (strokeY);

def penTraverse=
	pickup pencircle scaled (strokeY /2);
enddef;

def penCanne=
	pickup pencircle scaled (strokeY /2.7);
enddef;

def penDelie=
	pickup pencircle scaled (strokeY /3);
enddef;

% Les lignes ci-dessous permettent de dessiner la grille en faisant appel aux variables définies plus haut.
def beginchar(expr keycode, width)=
	beginfig(keycode);
		pickup pencircle scaled .2;

		draw (0 * ux, (descHeight - 2) * uy) -- 
			(width * ux, (descHeight - 2) * uy) --
			(width * ux, (ascHeight + 2) * uy) -- 
			(0 * ux, (ascHeight + 2) * uy) -- 
			cycle scaled 0 withcolor red;

		if grid = 1:
			defaultscale := .2;
			for i=0 upto width:
				draw (i*ux, ascHeight*uy) -- (i*ux, descHeight*uy) withcolor .3white;
			endfor;
			for i=descHeight upto (ascHeight):
				draw (width*ux, i*uy) -- (0*ux, i*uy) withcolor .3white;
			endfor;
		fi;
		pickup pencircle scaled 1;

		if hints =1:
			% draw (0 * ux, (xHeight * uy) + os_x) -- (width * ux, (xHeight * uy) + os_x)  withcolor green;
			% draw (0 * ux, xHeight * uy) -- (width * ux, xHeight * uy)  withcolor (green + blue);

			draw (0 * ux, capHeight * uy) -- (width * ux, capHeight * uy)  withcolor (green + blue);
			draw (0 * ux, ascHeight * uy) -- (width * ux, ascHeight * uy)  withcolor (green + blue);
			draw (0 * ux, descHeight * uy) -- (width * ux, descHeight * uy)  withcolor (green + blue);
			draw (0 * ux, baseline * uy) -- (width * ux, baseline * uy)  withcolor green;
		fi;
		% linejoin := beveled;
		linecap := rounded;
		pickup pencircle xscaled strokeX yscaled strokeY rotated rotation;

	enddef;

	% Pour s'y retrouver on peut matérialiser et numéroter les points du tracé.
	def endchar(expr lenDots)=
	if dot_label = 1:
		defaultscale := 3;
		for i=1 upto lenDots:
			dotlabels.urt([i]) withcolor blue;
		endfor;
	fi;
endfig;
enddef;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Ici commence la description d'une première figure 

%espace
beginchar(32,8)
endchar

%a
beginchar(97, 8)
	x1 := 2 *ux;
	y1 := 3.5 * uy;
	x2 := 2.5 * ux;
	y2 := 4.2* uy;
	x3:= 5.5 * ux;
	y3:= 4 *uy;
	x4:= 5.5 *ux;
	y4:= 1*uy;
	x5:= 6.5 *ux;
	bot y5:= 0*uy;
	x6:= 5.5 *ux;
	y6:= 3.5*uy;
	x7:= 1.8 *ux;
	y7:= 0.8*uy;
	x8:= 5.5 *ux;
	bot y8:= 2.1 *uy;
	draw z1..z2..z3--z4..z5;
	draw z6{down}..z7..{up}z8;

endchar(8)

%e
beginchar(101,8)
	x1:= 1*ux;
	y1:= 3*uy;
	x2:= 5*ux;
	y2:= 3*uy;
	x3:= 3.5*ux;
	y3:= 4.5*uy;
	x4:= 1*ux;
	y4:= 3*uy;
	x5:= 1*ux;
	y5:= 2*uy;
	x6= 3.5*ux;
	bot y6:= 0*uy;
	x7:= 5*ux;
	y7:= 1.5*uy;


	draw z1--z2..z3..{down}z4..z5..z6..z7;

endchar(7)

%f
beginchar(102,8)
	x1:= 2 *ux;
	bot y1:= 0 * uy;
	x2:= 2 * ux;
	top y2:= 9.5 * uy;
	x3:= 7 * ux;
	y3:= 9.5 * uy;
	x4:= 2 * ux;
	y4:= 4.5 * uy;
	x5:= 6.5 * ux;
	y5:= 4.5 * uy;

	draw z1--z2;
	draw z2{up}..z3;
	draw z4--z5;

endchar(5)



%l
beginchar(108,8)
	x1 := 2 * ux;
	bot y1 := baseline;
	x2 := 2 * ux;
	top y2 := 11 * uy;
	draw z1--z2;
endchar(2)

%t
beginchar(116,8)
	x2 := 4 * ux;
	y2 :=0.5*uy;
	x1 := 4 * ux;
	top y1:= 11 * uy;
	x3:= 2 *ux;
	y3:= 4.5 * uy;
	x4:= 6 * ux;
	y4:= 4.5 * uy;

	draw z1--z2;
	draw z3--z4;

endchar(4)

%u
beginchar(117,8)
	x1:=2*ux;
	top y1:=5*uy;

	x3:=2*ux;
	y3:=2*uy;
	x4:=4*ux;
	bot y4:=0*uy;
	x5:=6*ux;
	y5:=2*uy;
	x6:=6*ux;
	top y6:=5*uy;
	x7:= 6*ux;
	bot y7:= 0*uy;
	draw z1--z3{down}..z4..{up}z5--z6;
	draw z5--z7;

endchar(7)


%o
beginchar(111,8)
	z1 :=(6ux, 2.5uy);

	top z2 = (4ux, 5uy);
	z3 = (2ux, 2.5uy);
	z4 = (2ux, 2.5uy);
	x5 := 4ux;
	bot y5 := baseline;
	x6 := 6ux;
	y6 := 2.5uy;

	%x7 := 7ux;
	%y7 := 7.5uy;

	draw z1{up}..z2 .. {down}z3--z4{down}..z5..{up}z6--z1;
	pickup pencircle scaled (strokeX / 1.53);
endchar;


%j
beginchar(106,8)
	x1:= 3.5 * ux;
	y1:= 7 * uy;
	x2:= 3.5 * ux;
	y2:= 5 * uy;
	x3:= 3.5 * ux;
	y3:= 0 * uy;
	x4:= -0.5 * ux;
	y4:= 0 * uy;
	x5:= 1.5 * ux;
	y5:= -3 * uy;

	draw z1..cycle;
	draw z2--z3;
	draw z3{down}..z5;
endchar;

%p
beginchar(112,8)
	x1:= 1.5 * ux;
	bot y1:= 4 *uy;
	x2:= 1.5 * ux;
	bot y2:= -3 * uy;
	x3:= 1.5 * ux;
	y3:= 3 * uy;
	x4:= 6 * ux;
	y4:= 3 * uy;
	x5:= 6 * ux;
	y5:= 1.8 * uy;
	x6:= 1.5 * ux;
	y6:= 1.8* uy;
	x7:= 4 * ux;
	y7:= 4.5 * uy;


	draw z1--z2;
	draw z3..z7..z4;
	draw z4--z5;
	draw z6{down}..z5;
endchar;

%s
beginchar(115,8)
	x1:= 1.5 * ux;
	y1:= 3.5 * uy;
	x2:= 3.5 * ux;
	y2:= 4.5 * uy;
	x3:= 6.5 * ux;
	y3:= 3.5 * uy;
	x4:= 4 * ux;
	y4:= 2.5 * uy;
	x5:= 6.5 * ux;
	y5:= 1.5 * uy;
	x6:=  3.5 * ux;
	bot y6:= baseline * uy;
	x7:= 1.5 * ux;
	y7:= 1.5 * uy;

	draw z1..z2..z3;
	draw z1..z4..z5;
	draw z5..z6..z7;

endchar;









%n
beginchar(110,8)
	x1:= 1.5*ux;
	top y1:=5*uy;
	x2:=1.5*ux;
	bot y2:=0*uy;
	x3:=1.5*ux;
	y3:=3*uy;
	x4:=3.5*ux;
	top y4:=5*uy;
	x5:=5.5*ux;
	y5:=3*uy;
	x6:=5.5*ux;
	bot y6:=0*uy;
	draw z1--z2;
	draw z3{up}..z4..{down}z5--z6;

endchar;

%h
beginchar(104,8)
	x1 :=2 * ux;
	bot y1 := baseline * uy;
	x2 := 2 * ux;
	top y2 := 11 * uy;
	x3 := 2 * ux;
	y3 := 4 * uy;
	x4 := 6 * ux;
	bot y4 := baseline * uy;
	x5 := 6 * ux;
	y5 := 4 * uy;

	draw z1{up}--z2;
	draw z3..z5{dir 270}..z4;
endchar;

% A
% Pour spécifier qu'il s'agit de notre A capital latin, sa valeur ascii est indiquée, le 9 qui suit correspond à la largeur de la grille (nombre d'unités)

beginchar(65, 9)
	x1 := 2 * ux;
	bot y1 := 0 * uy;
	x2 := 4.5 * ux - (strokeX / 5);
	top y2 := capHeight * uy;
	x3 := 4.5 * ux + (strokeX / 5);
	top y3 := capHeight * uy;
	x4 := 7 * ux;
	bot y4 := 0 * uy;

	z5 := .15[z1,z2];

	z6 := .15[z4,z3];

	draw z1 -- z2{dir 20} .. z3 -- z4;
	% pickup pencircle traverse;
	penTraverse;
	draw z5 -- z6;

endchar(6);

%C
beginchar(67,8)
	x1 := 6ux;
	y1 := 7.5uy;
	top z2 = (4ux, 11uy);
	z3 = (2ux, 7.5uy);
	z4 = (2ux, 3.5uy);
	x5 := 4ux;
	bot y5 := baseline;
	x6 := 6ux;
	y6 := 3.5uy;

	%x7 := 7ux;
	%y7 := 3.5uy;

	%x8 := 7ux;
	%y8 := 7.5uy;

	%draw z8--z1{up}..z2 .. {down}z3--z4{down}..z5..{up}z6--z7;
	%pickup pencircle scaled (strokeX / 1.53);

	draw z1{up}..z2 .. {down}z3--z4{down}..z5..{up}z6;
	pickup pencircle scaled (strokeX / 1.53);

endchar(6);




%@ 
beginchar(64, 10)
	x1 := 7ux;
	y1 := 6.5uy;
	z2 = (5.5ux, 9uy);
	z3 = (4ux, 6.5uy);
	z4 = (4ux, 5uy);
	x5 := 5.5ux;
	y5 := 2.5uy;
	x6 := 7ux;
	y6 := 5uy;

	x7 := 6.5ux;
	y7 := 5uy;



	draw z1{up}..z2.. z3{down}--z4{down}..z5..z6{up}--z;

endchar(7);


endchar(6);


% K
beginchar(75, 10)
	x1 := 3ux;
	bot y1 := baseline;
	x2:=3ux;
	top y2:=capHeight * uy;
	x3:=7.5ux;
	top y3:=capHeight * uy;
	z4:=(3ux, 3uy);
	z5 := 0.5 [z3,z4];
	bot z6 := (7.5ux, baseline);
	bot y5:=baseline * uy;
	draw z1--z2;
	draw z3--z4;
	draw z5--z;
endchar(6);










% H
beginchar(72, 8);
	x1 := 2 * ux;
	bot y1 := 0 * uy;
	x2 := 2 * ux;
	top y2 := capHeight * uy;
	x3 := 6 * ux;
	top y3 := capHeight * uy;
	x4 := 6 * ux;
	bot y4 := 0 * uy;
	x5 := 2 * ux;
	y5 := 5.5 * uy;
	x6 := 6 * ux;
	y6 := 5.5 * uy;


	draw z1 -- z2;
	draw z3 -- z4;
	pickup pencircle scaled (strokeX / 1.5);
	draw z5 -- z6;

endchar(6);

%M
beginchar(77,12);
	x1 := 2 * ux;
	bot y1 := baseline * uy;
	x2 := 2 * ux;
	top y2 := capHeight * uy;
	x3 := 2 * ux + (strokeX / 5);
	top y3 := 11 * uy;
	x4 := 6 * ux - (strokeX / 8) ;
	bot y4 := baseline * uy;

	x5 := 6 * ux + (strokeX / 8);
	bot y5 := baseline * uy;
	x6 := 10 * ux - (strokeX / 5);
	top  y6 := capHeight * uy;
	x7 := 10 * ux;
	top  y7 := capHeight * uy;
	x8 := 10 * ux;
	bot y8 := baseline * uy;

	draw z1 -- z2;
	draw z6 -- z7 -- z8;
	draw z2 .. z3;

	draw z2--z3 -- z4 -- z5 -- z6;

endchar(8);

%T
beginchar(84, 8)
	x1 :=2 * ux;
	top y1 := capHeight * uy;
	x2 := 4 * ux;
	top y2 := capHeight * uy;
	x3 := 6 * ux;
	top y3 := capHeight * uy;
	x4 := 4 * ux;
	bot y4 := baseline * uy;

	draw z1 -- z3;
	draw z2 -- z4;

endchar(4);

%F
beginchar(70, 7)
	x1 := 2 * ux;
	bot y1 := baseline * uy;
	x2 := 2 * ux;
	top y2 := capHeight * uy;
	x3 := 5 * ux;
	top y3 := capHeight * uy; 
	x4 := 2 * ux;
	y4 := 5.5 * uy;
	x5 := 5* ux;
	y5 := 5.5 * uy;

	draw z1 -- z2 -- z3;
	pickup pencircle scaled (strokeX / 1.5);
	draw z4 -- z5;

endchar(5);

% N
beginchar(78, 10);
	x1 := 2 * ux;
	bot y1 := 0 * uy;
	x2 := 2 * ux;
	top y2 := capHeight * uy;
	x3 := 2 * ux + (strokeX / 3);
	top y3 := capHeight * uy;
	x4 := 8 * ux - (strokeX / 3);
	bot y4 := 0 * uy;
	x5 := 8 * ux;
	bot y5 := 0 * uy;
	x6 := 8 * ux;
	top y6 := capHeight * uy;

	draw z1 -- z2 --  z3 -- z4 -- z5 -- z6;

	pickup pencircle scaled (strokeX / 1.5);

endchar(6);

% O
beginchar(79, 8);

	z1 :=(6ux, 7.5uy);

	top z2 = (4ux, 11uy);
	z3 = (2ux, 7.5uy);
	z4 = (2ux, 3.5uy);
	x5 := 4ux;
	bot y5 := baseline;
	x6 := 6ux;
	y6 := 3.5uy;

	%x7 := 7ux;
	%y7 := 7.5uy;

	draw z1{up}..z2 .. {down}z3--z4{down}..z5..{up}z6--z1;
	pickup pencircle scaled (strokeX / 1.53);
endchar(6);

% Q
beginchar(81, 8);
	x1 := 6ux;
	y1 := 7.5uy;
	top z2 = (4ux, 11uy);
	z3 = (2ux, 7.5uy);
	z4 = (2ux, 3.5uy);
	x5 := 4ux;
	bot y5 := baseline;
	x6 := 6ux;
	y6 := 3.5uy;
	x7 := 6ux;
	y7 := 7.5uy;
	x8 := 4ux;
	y8 := baseline;
	x9 := 5ux;
	y9 := -2uy;




	draw z1{up}..z2 .. {down}z3--z4{down}..z5..{up}z6--z7;
	draw z8{down}..z9;

endchar(9);

%B
beginchar(66,8)

	x1:= 2 * ux;
	bot y1 := baseline * uy;
	x2 :=  2 * ux;
	top y2 := capHeight * uy;
	x3 := 4 * ux;
	top y3 := capHeight * uy;
	x4 := 4 * ux;
	y4 := 5.5 * uy;
	x5 := 4 * ux;
	bot y5 := baseline * uy;
	x6 := 6 * ux;
	y6 := 8.25 * uy;
	x7 := 6 * ux;
	y7 := 2.25 * uy;
	x8 := 2* ux;
	y8 := 5.5* uy;

	%draw z1--z2--z3{right}..z6..z4--z8;
	%draw z4{right}..z7..z5--z1;
	draw z5--z1--z2--z3;
	draw z3{right}..z6..{left}z4--z8;
	draw z4{right}..z7..{left}z5;

	pickup pencircle scaled (strokeX / 1.53); 

endchar(8)

%X
beginchar(88,8)

	x1 := 2 * ux;
	bot y1 := 0 * uy;
	x2 := 8 * ux;
	top y2 := capHeight * uy;
	x3:= 2 * ux;
	top y3:=capHeight * uy;
	x4:= 8 *ux;
	bot y4 := 0 * uy;

	draw z1--z2;
	draw z3--z4;
endchar(4)

%Z
beginchar(90,8)

	x1:= 2 * ux;
	top y1:= capHeight * uy;
	x2:= 7 * ux;
	top y2:= capHeight * uy;
	x3:= 2 * ux;
	bot y3:= 0 * uy;
	x4:= 7 * ux;
	bot y4:= 0 * uy;

	draw z1--z2--z3--z4;

endchar(4)

%.
beginchar(46,2)

	x1:= 1 * ux;
	bot y1:= baseline * uy;

	draw z1;
endchar(1)

%,
beginchar(44,8)

	x1:= 1 * ux;
	y1:= -3 * uy;
	x2:= 4 * ux;
	y2:= -1 * uy;
	x3:= 3 * ux;
	y3:= 3 * uy;
	x4:= 1.5 * ux;
	y4:= 1.5 * uy;
	x5 := 2.5 * ux;
	y5 := 0 * uy;

	draw z1..z2..z3..z4..z5..z1
		draw z1..cycle



			endchar(5)

			%?
			beginchar(63,6.5)

				x1:= 1 * ux;
				y1:= 8 * uy;
				top x2:= 3.5 * ux;
				top y2:= capHeight * uy;
				x3:= 5.5 * ux;
				y3:= 8 * uy;
				x4:= 2.25 * ux;
				y4:= 3.5 * uy;
				x5:= 2.25 * ux;
				y5:= 2 * uy;

				x6:= 2.25 * ux;
				bot y6:= baseline * uy;

				draw z1{up}..z2..z3{down}..z4{down}--z5;
				draw z6;
			endchar(6)


			%!
			beginchar(33,2)

				x1:= 1 * ux;
				top y1:= capHeight * uy;
				x2:= 1 * ux;
				y2:= 3 * uy;

				x3:= 1 * ux;
				bot y3:= baseline * uy;

				draw z1--z2;
				draw z3;
			endchar(3)


			%(
			beginchar(40,3)

				x1:= 2 * ux;
				top y1:= capHeight * uy;
				x2:= 1 * ux;
				y2:= 5.5 * uy;
				x3:= 2 * ux;
				bot y3:= baseline * uy;

				draw z1..z2..z3;

			endchar(3)

			%)
			beginchar(41,3)

				x1:= 1 * ux;
				top y1:= capHeight * uy;
				x2:= 2 * ux;
				y2:= 5.5 * uy;
				x3:= 1 * ux;
				bot y3:= baseline * uy;

				draw z1..z2..z3;


			endchar(3)

			%"
			beginchar(34,3)

				x1:= 1 * ux;
				top y1:= capHeight * uy;
				x2:= 1 * ux;
				y2:= 8.5 * uy;
				x3:= 3 * ux;
				top y3:= capHeight * uy;
				x4 := 3 * ux;
				y4 := 8.5 * uy;

				draw z1--z2;
				draw z3--z4;


			endchar(4)

			%/
			beginchar(47,6)

				x1 := 1.5 * ux;
				bot y1 := baseline * uy;
				x2 := 4.5 * ux;
				top y2 := capHeight * uy;

				draw z1--z2 ;

			endchar(2)

			% 0
			beginchar(48, 6.5);

				x1 := 5.5ux;
				y1 := 8uy;
				top z2 = (3.25ux, 11uy);
				z3 = (1ux, 8uy);
				z4 = (1ux, 3uy);
				x5 := 3.25ux;
				bot y5 := baseline;
				x6 := 5.5ux;
				y6 := 3uy;


				draw z1{up}..z2 .. {down}z3--z4 ;
				draw z1 -- z6 {down}..z5..{up}z4;
				pickup pencircle scaled (strokeX / 1.53);
			endchar(6);



			%1
			beginchar(49,6)

				x1:= 2 * ux;
				top y1:= capHeight * uy;
				x2:= 4 * ux;
				top y2:= capHeight * uy;
				x3:= 4 * ux;
				bot y3:= baseline * uy;

				draw z1--z2--z3;

			endchar(3)

			%2
			beginchar(50,8)
				x1 := 2 * ux;
				y1 := 8 * uy;
				x2 := 4.25 * ux;
				top y2 := capHeight * uy;
				x3 := 6.5 * ux;
				y3 := 8 * uy;
				x4 := 4.25 * ux;
				y4 := 5.5 * uy;
				x5 := 2 * ux;
				bot y5 := baseline * uy;
				x6 := 6.5 * ux;
				bot y6 := baseline * uy;

				draw z6--z5{up}..z4..z3{up}..z2..z1{down} ;
			endchar(6)

			%3
			beginchar(51,6.5)
				x1 := 1 * ux;
				y1 := 8 * uy;
				top x2 := 3.5 * ux;
				top y2 := capHeight * uy;
				x3 := 5.5 * ux;
				y3 := 8.25 * uy;
				x4 := 3 * ux;
				y4 := 5.5 * uy;
				x5 := 2.5* ux;
				y5 := 5.5* uy;
				x6:= 5.5 * ux;
				y6:= 3 * uy;
				bot x7:= 3 * ux;
				bot y7:= 0 * uy;
				x8:= 1 * ux;
				y8:= 3 * uy;

				draw z4{right} .. z3 .. z2 .. z1{down};
				draw z5--z4{right}..z6..z7..z8{up}

					endchar(8)

					%4
					beginchar(52,10)

						x1:= 7 * ux;
						bot y1:= 0 * uy;
						x2:= 7 * ux;
						top y2:= capHeight * uy;
						x3:= 2 * ux;
						y3:= 4 * uy;
						x4:= 9 * ux;
						y4:= 4 * uy;

						draw z1--z2--z3--z4;

					endchar(4)




					%7
					beginchar(55,8)
						x1:= 2 * ux;
						top y1:= capHeight * uy;
						x2 := 6.5 * ux;
						top y2:= capHeight * uy;
						x3:= 3 * ux;
						bot y3:= baseline * uy;

						draw z1--z2--z3;

					endchar(3)


					%8

					beginchar(56,10)

						x1 := 5 * ux;
						top y1 := capHeight * uy;
						x2 := 2.5 * ux ;
						y2 := 8.5 * uy ;
						x3 := 5 * ux;
						y3 := 6 * uy;
						x4 := 8 * ux;
						y4 := 3 * uy;
						x5 := 5 * ux;
						bot y5 := baseline * uy;
						x6 := 2 * ux;
						y6 := 3 * uy;
						x7 := 7.5 * ux;
						y7 := 8.5 * uy;

						draw z7{up}..{left}z1{left}..{down}z2{down}..{right}z3{right}..{down}z4{down}..{left}z5{left}..{up}z6{up}..{right}z3{right}..{up}z7{up}.. cycle; 

					endchar(7) ;

					%9
					beginchar(57,10)

						x1:= 6.5 * ux;
						y1:= 6.5 * uy;
						x2:= 4 * ux;
						y2:= 5 * uy;
						x3:= 2 * ux;
						y3:= 8 * uy;
						x4:= 3.5 * ux;
						top y4:= capHeight * uy;
						x5:= 6.5 * ux;
						y5:= 8 * uy;
						x6:= 6.5 * ux;
						y6:= 3 * uy;
						x7:= 4.5 * ux;
						bot y7:= baseline* uy;
						x8:= 2 * ux;
						y8:= 3 * uy;

						draw z1..z2{left}...z3..z4..z5{down}--z6{down}..{left}z7{left}..z8{up;

						endchar(8);




						beginchar(86,10)
							x1 := 2 * ux;
							top y1 := capHeight * uy;
							x2 := 5 * ux;
							bot y2 := baseline * uy;
							x3 := 8 * ux;
							top y3 := capHeight * uy;

							draw z1--z2--z3;

						endchar(3);

						%W

						beginchar(87,12)
							x1 := 2 * ux;
							top y1 := capHeight * uy;
							x2 := 4 * ux;
							bot y2 := baseline * uy;
							x3 := 4 * ux + (strokeX / 5);
							bot y3 := baseline * uy;
							x4 := 6 * ux;
							top y4 := capHeight * uy;
							x5 := 8 * ux;
							bot y5 := baseline * uy;
							x6 := 8* ux + (strokeX / 5);
							bot y6 := baseline * uy;
							x7 := 10 * ux;
							top y7 := capHeight * uy;


							draw z1--z2;
							draw z2--z3;
							draw z5--z6;
							draw z6--z7;
							pickup pencircle scaled (strokeX / 1);
							draw z3--z4--z5;

						endchar(7);

						%Y

						beginchar(89,8)
							x1 := 2 * ux;
							top y1 := capHeight * uy;
							x2 := 4 * ux;
							bot y2 := 2 * uy;
							x3 := 6 * ux;
							top y3 := capHeight * uy;
							x4 := 4 * ux;
							bot y4 := baseline * uy;

							draw z1--z2--z3;
							draw z2--z4;

						endchar(4);


						%L

						beginchar(76,8)
							x1 := 2 * ux;
							top y1 := capHeight * uy;
							x2 := 2 * ux;
							bot y2 := baseline * uy;
							x3 := 6 * ux;
							bot y3 := baseline * uy;

							draw z1--z2--z3;

						endchar(3);

						%U

						beginchar(85,8)
							x1 := 6ux;
							top y1 := capHeight * uy;
							x2 := 6ux;
							y2 := 3.5uy;
							x3 := 4ux;
							bot y3 := baseline;
							z4 = (2ux, 3.5uy);
							x5 := 2ux;
							top y5 := capHeight * uy;

							draw z1--z2{down}..z3..{up}z4--z5;
							pickup pencircle scaled (strokeX / 1.53);

						endchar(5);


						%D

						beginchar(68,8)
							x1 := 2 * ux;
							bot y1 := baseline * uy;
							x2 := 2 * ux;
							top y2 :=  capHeight * uy;
							x3 := 4.5 * ux;
							top y3 := capHeight * uy;
							x4 := 6.5 * ux;
							y4 := 8 * uy;
							x5 := 6.5 * ux;
							y5 := 3 * uy;
							x6 := 4.5 * ux;
							bot y6 := baseline * uy;

							draw z4{up}..{left}z3--z2--z1--z6{right}..{up}z5--z4;
						endchar(6);

						%

						beginchar(2192,8)
							x1 := 2 * ux;
							y1 := 3 * uy;
							x2 := 7 * ux;
							y2 := 3 * uy;
							x3 := 5 * ux;
							y3 := 5 * uy;
							x4 := 5 * ux;
							y4 := 1 * uy;

							draw z1--z2--z3;
							draw z2--z4;

						endchar(4);

						%

						beingchar(8594,8)
						x1 := 2 * ux;
						y1 := 2 * uy;
						x2 := 9 * ux;
						y2 := 2 * yx;

					endchar(2);



					%R
					beginchar(82,8)
						x1 := 2 * ux;
						bot y1 := 0 * uy;
						x2 := 2 * ux;
						top y2 := capHeight * uy;
						x3 := 4 * ux;
						top y3 := 11 * uy;
						x4 := 6 * ux;
						y4 := 8.5 * uy;
						x5 := 4 * ux;
						y5 := 6 * uy;
						x6 := 2 * ux;
						y6 := 6 * uy;
						z7 := 0.8 [ z6, z5];
						x8 := 6 * ux;
						y8 := 3.5 * uy;
						x9 := 6 * ux;
						bot y9 := 0 * uy;

						%x10 := 6.2 * ux;
						%bot y10 := 0 * uy;

						%draw z1--z2;
						%draw z2--z3{right}..z4..{left}z5--z6;
						%draw z7{right}..{down}z8--z9..z10{down};

						draw z1--z2;
						draw z2--z3{right}..z4..{left}z5--z6;
						draw z7{right}..{down}z8--z9;

					endchar(9)

					%P
					beginchar(80,10)
						x1 := 3 * ux;
						bot y1 := 0 * uy;
						x2 := 3 * ux;
						top y2 := capHeight * uy;
						x3 := 5 * ux;
						top y3 := 11 * uy;
						x4 := 7 * ux;
						y4 := 8 * uy;
						x5 := 5 * ux;
						y5 := 6 * uy;
						x6 := 3 * ux;
						y6 := 6 * uy;

						draw z1--z2;
						draw z2--z3{right}..z4..{left}z5--z6;

					endchar(6)


					%S
					beginchar(83,8)

						x1 := 4 * ux;
						top y1 := capHeight * uy;
						x2 := 1 * ux;
						y2 := 8.5 * uy;
						x3 := 4 * ux;
						y3 := 6 * uy;
						x4 := 7 * ux;
						y4 := 3 * uy;
						x5 := 4 * ux;
						bot y5 := baseline * uy;
						x6 := 1 * ux;
						y6 := 3 * uy; 
						x7 := 7 * ux;
						y7 := 8.5 * uy;

						draw z7{up}..{left}z1{left}..{down}z2{down}..{right}z3{right}..{down}z4{down}..{left}z5{left}..{up}z6;

					endchar(7);




					%I

					beginchar(73,6)
						x1 := 3 * ux;
						top y1 := 11 * uy;
						x2 := 3 * ux;
						bot y2 := 0 *uy;

						draw z1--z2;

					endchar(2);

					%E
					beginchar(69,7)
						x1 := 2 * ux;
						bot y1 := 0 * uy;
						x2 := 2 * ux;
						y2 := 5.5 *uy;
						x3 := 2 * ux;
						top y3 := 11 *uy;
						x4 := 5 * ux;
						top y4 := 11 *uy;
						x5 := 5 * ux;
						y5 := 5.5 *uy;
						x6 := 5 * ux;
						bot y6 := 0 *uy;

						draw z6 -- z1--z2--z3 -- z4;
						pickup pencircle scaled (strokeX / 1.5);
						draw z2--z5;

					endchar(6)

					%accent aigue
					beginchar(714,7)
						x7 :=3 * ux;
						y7 := 12 * uy;
						x8 :=4 * ux;
						y8 :=13 * uy;


						draw z7--z8;

					endchar(2)

					%^
					beginchar(710,7)
						x1 := 1 * ux;
						y1 := 12 * uy;
						x1 := 2 * ux;
						bot y1 := 0 * uy;
						x2 := 2 * ux;
						y2 := 5.5 *uy;
						x3 := 2 * ux;
						top y3 := 11 *uy;
						x4 := 5 * ux;
						top y4 := 11 *uy;
						x5 := 5 * ux;
						y5 := 5.5 *uy;
						x6 := 5 * ux;
						bot y6 := 0 *uy;

						draw z6 -- z1--z2--z3 -- z4;
						pickup pencircle scaled (strokeX / 1.5);
						draw z2--z5;
					endchar(5)

					%accent grave
					beginchar(710,7)

						x7 := 3 * ux;
						y7 := 13 * uy;
						x8 := 4 * ux;
						y8 := 12 * uy;
						draw z7--z8;

					endchar(8)

					%É
					beginchar(201,7)

						x1 := 2 * ux;
						bot y1 := 0 * uy;
						x2 := 2 * ux;
						y2 := 5.5 *uy;
						x3 := 2 * ux;
						top y3 := 11 *uy;
						x4 := 5 * ux;
						top y4 := 11 *uy;
						x5 := 5 * ux;
						y5 := 5.5 *uy;
						x6 := 5 * ux;
						bot y6 := 0 *uy;

						x7 :=3 * ux;
						y7 := 11 * uy;
						x8 :=4 * ux;
						y8 :=12 * uy;

						draw z6 -- z1--z2--z3 -- z4;
						pickup pencircle scaled (strokeX / 1.5);
						draw z2--z5;
						pickup pencircle scaled (strokeX * 1);
						draw z7--z8;
					endchar


					%J
					beginchar(74,9)
						x1 := 6.5 * ux;
						top y1 := capHeight * uy;
						x2:= 6.5 * ux;
						y2:= 3.5 * uy;
						bot x3:= 2.75 * ux;
						bot y3:= 0 * uy;
						x4:= 2 * ux;
						y4:= 3.5 * uy;

						draw z1 -- z2{down}..z3..z4{up};

					endchar(4);

					%G
					beginchar(71,8)
						x1:= 6 * ux;
						y1 := 7 * uy;
						x2 := 6 * ux;
						top y2 := 8.5 * uy;
						x3 := 4 * ux;
						top y3 := capHeight * uy;
						top y4 := 8.5 *uy;
						x4 := 2 * ux;
						x5 := 2 * ux;
						bot y5 := 2.5 * uy;
						x6 := 4 * ux;
						bot y6 := baseline * uy;
						x7 := 6 * ux;
						bot y7 := 2.5 * uy;
						x8 := 6 * ux;
						y8 := 4 * uy;
						x9 := 5 * ux;
						y9 := 4 * uy;
						x10 :=  6 * ux + (strokeX / 3);
						bot y10 := 0 * uy - (strokeY / 3.6);
						x11 := 6 * ux + (strokeX / 3);
						y11 := 4 * uy;
						x12 := 5 * ux + (strokeX / 3);
						y12 := 4 * uy + (strokeY / 3);

						draw z1--z2 {up}..z3 ..{down}z4 --z5 {down}..z6 ..{up}z7 --z8 --z9;
						draw z8 --z9;
						penCanne;
						draw z10--z11 -- z12;

					endchar(12)


					%5
					beginchar(53,8)
						x1 := 6.5 * ux;
						top y1:= capHeight * uy;
						x2:= 2.7 * ux;
						top y2:= capHeight * uy;
						x3:= 2 * ux;
						y3:= 6 * uy;
						x4:= 4.5 * ux;
						y4:= 7 * uy;
						x5:= 6.5 * ux;
						y5:= 3.5 * uy;
						bot x6:= 4 * ux;
						bot y6:= 0 * uy;
						x7:= 2 * ux;
						y7:= 3.5 * uy;

						draw z1--z2--z3..z4{right}..z5..z6..z7{up};
					endchar(7)

					% 6
					beginchar(54, 6.5);
						x1:= 1 * ux;
						y1:= 5 * uy;
						x2:= 3.5 * ux;
						y2:= 7 * uy;
						x3:= 5.5 * ux;
						y3:= 4 * uy;
						bot x4:= 2.5 * ux;
						bot y4:= 0 * uy;
						x5:= 1 * ux;
						y5:= 3.5 * uy;
						x6:= 1 * ux;
						y6:= 8 * uy;
						top x7:= 4 * ux;
						top y7:= capHeight * uy;
						x8:= 5.5 * ux;
						y8:= 8 * uy;

						draw z1{up}..z2{right}..z3..z4..z5{up}--z6{up}..z7..z8{down};
					endchar(8);


					%{
					beginchar(123,8)
						pickup pencircle
							z1 := (5ux, 11uy);
							z2 := (5ux, 0uy);
							z3 := (3ux, 5uy);
							z4 := (3ux, 4uy);

							draw zup}..z3..z4..z2;



						endchar(4);







						%[
						beginchar(91,7)


							top z1 := (5ux, capHeight * uy);
							top z2 :=(3ux, capHeight * uy);
							z3 := (3ux, 5.5uy);
							bot z4 :=  (3ux, baseline);
							bot z5 := (5ux, baseline);


							draw z1--z2--z3--z4--z5;

						endchar(5);

						%]
						beginchar(93,6)


							top z1 := (3ux, capHeight * uy);
							top z2 :=(5ux, capHeight * uy);
							z3 := (5ux, 5.5uy);
							bot z4 :=  (5ux, baseline);
							bot z5 := (3ux, baseline);


							draw z1--z2--z3--z4--z5;

						endchar(5);



						%b

						beginchar(98,8);

							x1 := 1.5 * ux;
							bot y1 := baseline;
							x2 := 1.5 * ux;
							top y2 := 11 * uy;
							x3 := 1.5 * ux;
							y3 :=  2.5 * uy;
							x4 := 4.5 * ux;
							top y4 := 5 * uy;
							x5 := 6.5 * ux;
							y5 := 2.5 * uy;
							x6 := 4.5 * ux;
							bot y6 := baseline;
							%x7 := 2 * ux;
							%y7 := 1 * uy;

							draw z1--z2--z3;
							draw z3{up}..z4..{down}z5..z6..cycle;


						endchar;

						%d

						beginchar(100,10);

							x1 := 8 * ux;
							bot y1 :=baseline * uy;
							x2 := 8 * ux;
							top y2 := 11 * uy;
							x3 := 3 * ux;
							y3 :=  2.5 * uy;
							x4 := 5 * ux;
							top y4 := 5 * uy;
							x5 := 8 * ux;
							y5 := 2.5 * uy;
							x6 := 5 * ux;
							bot y6 := baseline;
							%x7 := 2 * ux;
							%y7 := 1 * uy;

							draw z1--z2;
							draw z3{up}..z4..z5..z6.. cycle;


						endchar;

						%c

						beginchar (99,7)

							x1 := 5.5ux;
							bot y1 := 3uy;
							top z2 = (3.5ux, 5uy);
							z3 = (1.5ux, 2.5uy);
							x4 := 3.5ux;
							bot y4 := baseline;
							x5 := 5.5ux;
							top y5 := 2uy;

							draw z1{up}..z2..z3{down}..z4..{up}z5;
							pickup pencircle scaled (strokeX / 1.53);


						endchar;

						%v
						beginchar(118,10)
							x1 := 3 * ux;
							top y1 := 5 * uy;
							x2 := 5 * ux;
							bot y2 := baseline * uy;
							x3 := 7 * ux;
							top y3 := 5 * uy;

							draw z1 --z2 --z3;
						endchar;

						%w
						beginchar(119, 12)
							x1 := 2 * ux;
							top y1 := 5 * uy;
							x2 := 4 * ux;
							bot y2 := baseline * uy;
							x3 := 6 * ux;
							top y3 := 5 * uy;
							x4 := 8 * ux;
							bot y4 := baseline * uy;
							x5 := 10 * ux;
							top y5 := 5 * uy;
							draw z1 --z2 --z3 --z4 --z5;
						endchar;

						%x
						beginchar(120,10)

							x1 := 3 * ux;
							bot y1 := baseline;
							x2 := 7 * ux;
							top y2 := 5 * uy;
							x3 := 7 * ux;
							bot y3 := baseline;
							x4 := 3 * ux;
							top y4 := 5 * uy;

							draw z1--z2;
							draw z3--z4;

						endchar;

						%z

						beginchar (122,10)

							x1 := 3 * ux;
							top y1 := 5 * uy;
							x2 := 7 * ux;
							top y2 := 5 * uy;
							x3 := 3 * ux;
							bot y3 := baseline;
							x4 := 7 * ux;
							bot y4 := baseline;

							draw z1--z2--z3--z4;

						endchar;

						%y
						beginchar(121, 10)
							x1 := 3 * ux;
							top y1 := 5 * uy;
							x2 := 8.5 * ux;
							top y2 := 5 *  uy;
							x3 := 4 * ux;
							bot y3 := -3 * uy;
							x4 := 5.5 * ux;
							bot y4 := 0.5 * ux;
							draw z2 --z3;
							draw z1 --z4;
						endchar;

						%g
						beginchar(103,8)
							x1 := 6.5ux;
							bot y1 := 2.5uy;
							top z2 = (4*ux, 5 * uy);
							z3 = (1.5ux, 2.5uy);
							x4 := 2.9 * ux;
							bot y4 := 0.2 * uy;
							x5 := 6.5ux;
							top y5 := 2.5uy;
							x6 := 6.5 * ux;
							top y6 := 5 * uy;
							x7 := 6.5 * ux;
							bot y7 := -1.5 * uy;
							x8 := 1.5 * ux;
							bot y8 := -1.5 * uy;

							draw z1{up}..z2..z3{down}..z4..{up}z5 --z6 --z7{down} ..z8;
							pickup pencircle scaled (strokeX / 1.53);
						endchar;

						%q
						beginchar(113,10)
							x1:= 6.5 * ux;
							y1:= 4.5 * uy;
							x2:= 6.5 * ux;
							bot y2:= -3 * uy;
							x3:= 6.5 * ux;
							y3:= 3 * uy;
							x4:= 4 * ux;
							y4:= 4.5 * uy;
							x5:= 2 * ux;
							y5:= 3 * uy;
							x6:= 2 * ux;
							y6:= 1.8 * uy;
							x7:= 6.5 * ux;
							y7:= 1.8 * uy;

							draw z1--z2;
							draw z3..z4..z5;
							draw z5--z6;
							draw z6{down}..z7;
						endchar;

						%r
						beginchar(114,10)
							x1 := 1.5* ux;
							bot y1 := 2uy;
							top z2 = (5ux, 4.8uy);
							z3 = (5.7ux, 3.5uy);
							x4 := 5ux;
							bot y4 := baseline;
							x5 := 1.5ux;
							top y5 := 2uy;
							x6 := 1.5 * ux;
							top y6 := 5 * uy;
							x7 := 1.5 * ux;
							bot y7 := 0 * uy;

							draw z1{up}..z2..z3;
							draw z6 --z7;
							pickup pencircle scaled (strokeX / 1.53);
						endchar;

						%k
						beginchar(107,8)
							x1 := 1.5 * ux;
							bot y1 := baseline * uy;
							x2 := 1.5 * ux;
							top y2 := 11 * uy;
							x3 := 1.5 * ux;
							bot y3 := 3 * uy;
							x4 := 6.5 * ux;
							bot y4 := baseline * uy;
							x5 := 6.5 * ux;
							bot y5 := 3 * uy;
							x6 := 5 * ux;
							bot y6 := 1.5 * uy;
							x7 := 1.5 * ux;
							bot y7 := 2 * uy;
							draw z1{up}--z2;
							draw z7 {up}..z5 {down}..z6;
							draw z3--z4;

						endchar;

						%i
						beginchar(105,5)

							x1 := 2 * ux;
							bot y1 := baseline;
							x2 := 2 * ux;
							y2 := 5 * uy;
							x3 := 2 * ux;
							y3 := 7 * uy;

							draw z1--z2;
							draw z3..cycle;

						endchar;

						%m
						beginchar(109,14)
							x1:= 1.5*ux;
							top y1:=5*uy;
							x2:=1.5*ux;
							bot y2:=0*uy;
							x3:=1.5*ux;
							y3:=3*uy;
							x4:=3.5*ux;
							top y4:=5*uy;
							x5:=5.5*ux;
							y5:=3*uy;
							x6:=5.5*ux;
							bot y6:=0*uy;
							x7:=7.5*ux;
							top y7:=5*uy;
							x8:=9.5*ux;
							y8:=3*uy;
							x9:=9.5*ux;
							bot y9:=0*uy;
							draw z1--z2;
							draw z3{up}..z4..{down}z5--z6;
							draw z8--z9;
							draw z5{up}..z7..{down}z8;

						endchar;


						%ne touchez pas au "end" ci-dessous svplease
					end;








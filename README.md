# Meta-Elastique

![](images/caslongue-specimen-page001.png)


Le répertoire rassemble les fichiers sources et fontes dessinées à l'occasion du workshop *Meta-elastique* initié à la *Winter School* de l'erg, du 21 au 23 janvier 2020, avec Antoine Gelgon et Ludivine Loiseau.
Les sources et fontes du projet sont ici publiées sous licence [Open Font License](https://scripts.sil.org/cms/scripts/page.php?item_id=OFL_web).

## Description du workshop
Dérivé de Metafont, Metapost est un langage de programmation open source qui permet de dessiner des figures et par extension des polices de caractères. À l’opposé des principaux logiciels de dessins de lettres modélisant celles-ci par leur contours, Metapost décrit la lettre par son “ductus”, c’est-à-dire son chemin central ou squelette. Repartant du geste du lettreur, cet outil propose une conception digitale et paramétrique du caractère.

En point de départ de l’atelier, le premier caractère métallique arrondi édité en Europe, le Four-Line Rounded de la fonderie Caslon sur lequel nous tombons en feuilletant une *Histoire visuelle de l'art typographique / 1405-2015*, Paul McNeil, Actes Sud.
Cette *gothic rounded* est présente dans le *[Specimen of Printing types](https://archive.org/details/SpecimenOfPrintingTypes/page/n1
)* de la fonderie Caslon, publié en 1841.

La collection de fontes dessinées pendant le workshop porte pour nom dénominateur commun *Caslongue*.

À partir d’un même set de capitales décrit collectivement le premier jour, le *Caslongue-Fourline*, chaque groupe a dessiné sa propre variante.

*Bosobiso*, en biseau sharp et slalomé, par Clara Sambot et Simon Bouvier.

*Metaveste*, avec bas de casse décondensées par Apolline Verhasselt, Barney Rendace, Victor Le Roy et Damien Sard.

*Elasticgirls* où la grille de dessin devient courbes avec Ayasha Khan, Léna Salabert, Laura Conant.

*Bodybolder* en copeaux de métal fat par Mélina Vapici, Marc-Olivier Leroux, Stijn Vanrenterghem.

*Metacursive* italique 3D renversée par Émile Feyeux.

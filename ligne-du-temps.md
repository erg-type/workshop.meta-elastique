# Proposition programme

## mardi 21 janvier

* 9h30 : Introduction à *Metapost*

* 9H50 : Introduction élastique : un specimen de départ et quelques situations de lettrage en contextes avec contraintes physique

* 10h30 : Pause de 10mn

* 10h45 : Cheatsheet et présentation du set-up (adresse locale, écriture collective en pads)

* 11h30 : Chacun.e dessine un glyphe.

* 12h30 : Pause repas

* 13h30 : dessin des 26 glyphes latin par groupes (droites, rondes, diagonales, chiffres)

* 15h30 : Mise en commun et négociations vers des variables communes

## mercredi 22 janvier

* 9h30 : Mise en applications des décisions négociées : écrire de variables et de fonctions communes

* 11h30 : Dessin d'autres glyphes (ponctuation, figures signalétiques?)

* 12h30 : pause repas

* 13h30 : Tests élastiques : interaction avec la grille et les variables en présence pour tester et visualiser les extrèmes possibles (étirements horizontaux et verticaux)

* 14h30 : Ajustements des glyphes en réactions aux torsions élastiques

* 15h30 : Travail de lettrage à partir des espaces du plan de salles et compostion d'un premier poster specimen variables

* 16h30 : Discussion et choix de versions à fixer

## jeudi 23 janvier

* 9h30 : mise en place de 3 groupes de travail :
    - gérération, ajustements et mise en ligne des graisses choisies
    - dessin de specimens
    - réflexion sur une/des applications physiques possibles

* 13h30 : installation de metapost en locale sur les ordinateurs des participants

* 16h : ouverture et présentation au passants 

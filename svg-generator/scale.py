import os
import re
import sys 
import subprocess
import svgutils as su
import lxml.etree as et
from svg.path import parse_path

s = sys.argv[3]

print(sys.argv[1])

def proper_round(num, dec=0):
    num = str(num)[:str(num).index('.')+dec+2]
    if num[-1]>='5':
        return float(num[:-2-(not dec)]+str(int(num[-2-(not dec)])+1))
    return float(num[:-1])

def getInfoPath(f, pattern):
    file = open(f, "r")
    elem = et.parse(file)
    root = elem.getroot()
    for child in root:
        for child in root:
            if child.tag == '{http://www.w3.org/2000/svg}path':
                if child.attrib['style'].startswith(pattern):
                    dparse = parse_path(child.attrib['d'])
                    for point in dparse:
                        type = str(point)[0:4]
                        x = dparse[1].start.real
                        y = dparse[2].end.imag * -1
                        h = dparse[1].start.imag
                        w = dparse[1].end.real
    return [x, y, w, h]

def exeMP():
    Command = "mpost -interaction=batchmode mp/base.mp"
    process = subprocess.Popen(Command.split(), stdout=subprocess.PIPE)
    output, error = process.communicate()
    
def changeValue(x,y):
    file = open('mp/unity.mp', 'w')
    file.write('ux=' + str(x) + ' * u; uy=' + str(y) + ' * u;')
    file.close()

def build():
    width = sys.argv[1].split('=')[1]
    height = sys.argv[2].split('=')[1]
    changeValue(1,1)
    exeMP()
    
    x = 0
    y = 0
    for c in s:
        l = str(ord(c))
        svg = "svg/" + l + ".svg" 
        info = getInfoPath(svg, 'stroke:rgb(100.000000%,0.000000%,0.000000%);')
        x = x + info[2]
        y = info[3]

    print(y)
    newX = proper_round(int(width) / x, 2)
    newY = proper_round(int(height) / y, 2)
    print(newY)

    changeValue(newX,newY)
    exeMP()

    OUT = su.transform.SVGFigure(width, height)
    i = 0
    t = []
    x = 0
    for c in s:
        l = str(ord(c))
        svg = "svg/" + l + ".svg" 
        fig = su.transform.fromfile(svg)
        t.append(fig.getroot())
        info = getInfoPath(svg, 'stroke:rgb(100.000000%,0.000000%,0.000000%);')
        # print(info)
        t[i].moveto(x, 0, scale=1)
        x = x + info[2]
        OUT.append(t[i])
        i = i + 1
    OUT.save('output/' + s + '-' + width + '-' + height + '.svg')

build()
